package tests;

import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.hamcrest.*;
import org.hamcrest.MatcherAssert;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import classes.Links;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AssignmentTest {

	String userId = "podanimesh@gmail.com";
	String password = "Testuser123";
	WebDriver driver;

	 @Test
	 public void cartWorkflow() throws IOException{
	 ClassLoader classLoader = getClass().getClassLoader();
	 File payload = new
	 File(classLoader.getResource("payload.json").getFile());
	 Response postResponse =
	 RestAssured.with().accept("application/json").with().contentType("application/json").with().body(payload).request(Method.POST,
	 "http://t-dtap.api.albumprinter.com/OnlineshopApi/CartItems");
	 String cartId = postResponse.body().jsonPath().getString("CartID");
	 Assert.assertNotNull(cartId, "cart id null");
	 Assert.assertEquals(201, postResponse.getStatusCode(), "Response code is not 201");	
	 Response getResponse = RestAssured.with().accept("application/json").with().contentType("application/json").get("http://t-dtap.api.albumprinter.com/OnlineshopApi/Carts/" + cartId);
	 JsonPath jsonData = getResponse.jsonPath();
	
	 Assert.assertEquals(jsonData.getString("Channel"), "albelli.nl", "Incorrect Channel Name");
	 Assert.assertEquals(jsonData.getString("CartItems[0].Template.PredesignedProductID"), "a13db0ae-8225-4e8e-ac35-7326aad2d7f6" , "Incorrect Template Id");
	 Assert.assertEquals(jsonData.getString("ID"), cartId, "Invalid Cart ID");
	//Deserializing links object
	 List<Links> links = jsonData.getList("Links", Links.class);
	 Assert.assertEquals(links.size(), 4, "Incorrect size of links");
	 Assert.assertEquals(jsonData.getString("CartItems[0].Configuration.Article.ID"),  "PAP_050");
	
	
	
	 }

	@Test
	public void UITest() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "//Users//Animesh//bin//chromedriver2");

		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://bonusprint.co.uk");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//*[@href='/photo-books']"))).build().perform();
		driver.findElement(By.xpath("//*[@href='/start']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".button.choose-editor-flow-online")))
				.click();
		WebElement productList = driver.findElement(By.cssSelector(".products"));
		List<WebElement> list = productList.findElements(By.className("productbox_li"));
		WebElement item = list.get(0);
		action.moveToElement(item).build().perform();
		Thread.sleep(500);
		item.click();

		// Wait for Login popup
		WebElement loginDialog;
		try {
			loginDialog = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dialog-window.auth-window")));
		} catch (NoSuchElementException ex) {
			throw new TestException("Login popup did not load in 2 mins");
		}
		WebElement loginIframe = loginDialog.findElement(By.tagName("iframe"));
		//Switch to iFrame
		driver.switchTo().frame(loginIframe);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("Email"))).sendKeys(userId);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.id("loginButton")).click();
		driver.switchTo().defaultContent();
		//Handle popups
		wait.until(ExpectedConditions.elementToBeClickable(By.className("cancel"))).click();
		driver.findElement(By.cssSelector(".close.icon-exit")).click();
		//Upload dummy image
		driver.findElement(By.xpath("//*[@type='file']")).sendKeys("//Users//Animesh//Downloads//IMG_7997.JPG");
		WebElement uploadedImage = driver.findElement(By.cssSelector(".asset-list-item.photo-list-item"));
		WebElement canvas = driver.findElement(By.id("productCanvas"));
		//Wait for file to get uploaded
		Thread.sleep(2000);
		//Drag and Drop Image
		action.dragAndDrop(uploadedImage, canvas).build().perform();
		driver.findElement(By.id("saveProduct")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("input-saveName")))
				.sendKeys("Auto" + new Timestamp(System.currentTimeMillis()));
		driver.findElement(By.id("saveProductConfirmationButton")).click();
		//Wait for notification
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("notification")));
		driver.quit();
	
	}


}
